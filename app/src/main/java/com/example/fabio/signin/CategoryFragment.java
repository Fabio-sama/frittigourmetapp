package com.example.fabio.signin;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class CategoryFragment extends Fragment {
    private static final String NAMES_LIST = "names_list";
    private static final String PRICES_LIST = "prices_list";
    private static final String IDS_LIST = "ids_list";
    private CategoryAdapter adapter;
    private boolean is_admin;

    private ArrayList<Product> categoryList = new ArrayList<Product>();

    private OnFragmentInteractionListener mListener;

    public static CategoryFragment newInstance(ArrayList<Product> arrayList) {
        CategoryFragment fragment = new CategoryFragment();
        fragment.categoryList = arrayList;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragmentRoot = inflater.inflate(R.layout.fragment_category, container, false);
        adapter = new CategoryAdapter(getContext(), categoryList, this);
        ListView lv = fragmentRoot.findViewById(R.id.catalogue_fragment_listview);
        lv.setAdapter(adapter);

        return fragmentRoot;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            is_admin = mListener.onFragmentInteraction();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(int id, int pos);
        boolean onFragmentInteraction();
    }



    public void addToCart(int id, int quantity) {
        if (mListener != null) {
            mListener.onFragmentInteraction(id, quantity);
        }
    }

    public boolean get_is_admin() {
        return is_admin;
    }
}
