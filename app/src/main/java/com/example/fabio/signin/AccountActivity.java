package com.example.fabio.signin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class AccountActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        JSONObject jobj = retrieveData();

        TextView username = findViewById(R.id.account_username);
        TextView firstname = findViewById(R.id.account_firstname);
        TextView lastname = findViewById(R.id.account_lastname);
        TextView company = findViewById(R.id.account_company);
        TextView piva = findViewById(R.id.account_piva);
        TextView phone = findViewById(R.id.account_phone);
        TextView email = findViewById(R.id.account_email);
        TextView address = findViewById(R.id.account_address);
        ImageView catalogue = findViewById(R.id.upper_bar_catalogue);
        ImageView cart = findViewById(R.id.upper_bar_cart);

        catalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AccountActivity.this, CatalogueActivity.class);
                i.putExtra(CatalogueActivity.FRAG_TO_SET, CatalogueActivity.CATALOGUE_FRAG);
                startActivity(i);
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AccountActivity.this, CatalogueActivity.class);
                i.putExtra(CatalogueActivity.FRAG_TO_SET, CatalogueActivity.CART_FRAG);
                startActivity(i);
            }
        });

        try {
            /* Mostriamo tutti i campi salvati nelle preferenze */
            username.setText(jobj.getString("username"));
            firstname.setText(jobj.getString("name"));
            lastname.setText(jobj.getString("surname"));
            company.setText(jobj.getString("company"));
            piva.setText(jobj.getString("piva"));
            phone.setText(jobj.getString("phone"));
            email.setText(jobj.getString("email"));
            address.setText(jobj.getString("address"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject retrieveData() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        JSONObject jobj = new JSONObject();

        try {
            jobj.put("name", sharedPref.getString("name", null));
            jobj.put("surname", sharedPref.getString("surname", null));
            jobj.put("company", sharedPref.getString("company", null));
            jobj.put("piva", sharedPref.getString("piva", null));
            jobj.put("phone", sharedPref.getString("phone", null));
            jobj.put("email", sharedPref.getString("email", null));
            jobj.put("username", sharedPref.getString("username", null));
            jobj.put("address", sharedPref.getString("address", null));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jobj;
    }
}
