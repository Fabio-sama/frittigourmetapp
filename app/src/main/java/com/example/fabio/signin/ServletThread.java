package com.example.fabio.signin;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServletThread extends Thread {

    final static String LOGIN = "login";
    final static String CUSTOMERS_LIST = "customers";
    final static String REGISTER = "register";
    final static String PRODUCTS_LIST = "catalogue";
    final static String SERVLET_THREAD_LABEL = "ServerResponse";

    private final String URL = "http://192.168.1.6:8080/FrittiGourmetServlet/";

    private String bodyPOSTRequest;
    private Handler handler;
    private String servlet;

    public ServletThread(String servlet_name, String data, Handler handler){
        this.bodyPOSTRequest = data;
        this.handler = handler;
        this.servlet = servlet_name;
    }


    @Override
    public void run() {

        try {

            URL url = new URL(URL + servlet);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(5000 /* milliseconds */);
            conn.setConnectTimeout(5000 /* milliseconds */);

            conn.setRequestMethod("POST");
            conn.setDoInput(true); //su questa connessione voglio poter inviare dati
            conn.setDoOutput(true); //e anche riceverli

            OutputStream os = conn.getOutputStream(); //apro output stream per poter scrivere dei dati
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(bodyPOSTRequest); //scrivo la stringa passata al thread (un JSON serializzato)

            writer.flush(); //svuoto buffer writer
            writer.close(); //chiudo oggetto writer e os
            os.close();


            int responseCode = conn.getResponseCode();

            /* Se è andata a buon fine la comunicazione con il server */
            if (responseCode == HttpURLConnection.HTTP_OK) {

                /* Riceviamo la risposta del server */
                BufferedReader in=new BufferedReader(
                        new InputStreamReader(
                                conn.getInputStream(), "UTF-8"));
                String bodyPOSTResponse = "";
                String line = in.readLine();

                while(line != null) {

                    bodyPOSTResponse += line;
                    line = in.readLine();
                }

                in.close();

                //invio il messaggio ricevuto dal server al mainthread
                sendToMain(bodyPOSTResponse);
            }
            else {
                /* Arrivo qui se il server mi risponde qualcosa di diverso da 200 OK */
                sendToMain(null);
            }
        } catch (IOException e) {
            e.printStackTrace();
            sendToMain(null);
        }

    }

    public void sendToMain(String str) {
        Message msg = handler.obtainMessage();
        Bundle b = new Bundle();
        b.putString(SERVLET_THREAD_LABEL, str);
        msg.setData(b);
        handler.sendMessage(msg);
    }

}
