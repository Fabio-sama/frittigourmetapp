package com.example.fabio.signin;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class CustomersAdapter extends ArrayAdapter {

    private ArrayList<JSONObject> jobjArr;
    private int expandedPosition = -1;
    private CustomersActivity activity;

    public CustomersAdapter (Context context, ArrayList<JSONObject> objects, CustomersActivity activity) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.jobjArr = objects;
        this.activity = activity;

    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        JSONObject jobj_tmp = jobjArr.get(position);
        final String[] fields = new String[jobj_tmp.length()];

        /* Variabili per gestire il toggle sul listView item */
        View v;
        int h;
        ConstraintLayout cl;
        ViewGroup.LayoutParams lp;
        String unknown = getContext().getResources().getString(R.string.unknown_customers_field);


        try {
            fields[0] = jobj_tmp.getString("company");
            fields[1] = jobj_tmp.getString("phone");
            fields[2] = jobj_tmp.getString("name");
            fields[3] = jobj_tmp.getString("surname");
            fields[4] = jobj_tmp.getString("email");
            fields[5] = jobj_tmp.getString("piva");
            fields[6] = jobj_tmp.getString("address");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        /* Se ho già questa view, la "riciclo" senza crearne una nuova */
        if (convertView != null) {
            v = convertView;
        } else {
            LayoutInflater li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.customers_listview_item, parent, false);
        }

        cl = v.findViewById(R.id.customers_listview_layout);
        if (position%2 == 0) {
            cl.setBackgroundResource(R.color.cart_listview_light);
        } else {
            cl.setBackgroundResource(R.color.cart_listview_dark);
        }

        if (fields[0].compareTo("null") == 0) {

            TextView telephone_tv = v.findViewById(R.id.customers_listview_field2);

            /* L'utente è solo PRE-APPROVATO e di conseguenza ha i campi a null tranne l'email */
            ((TextView) v.findViewById(R.id.customers_listview_field1)).setText(unknown);
            ((TextView) v.findViewById(R.id.customers_listview_field2)).setText(unknown);
            ((TextView) v.findViewById(R.id.customers_listview_field3)).setText(unknown);
            ((TextView) v.findViewById(R.id.customers_listview_field4)).setText(unknown);
            ((TextView) v.findViewById(R.id.customers_listview_field5)).setText(fields[4]);
            ((TextView) v.findViewById(R.id.customers_listview_field6)).setText(unknown);
            ((TextView) v.findViewById(R.id.customers_listview_field7)).setText(unknown);

            /* Se non ho disponibile il dato della company, allora i campi saranno unknown (tranne la mail)
             * e , non ha senso attivare la chiamata sul click del numero telefonico */
            if (telephone_tv.hasOnClickListeners()) telephone_tv.setOnClickListener(null);

        } else {
            ((TextView) v.findViewById(R.id.customers_listview_field1)).setText(fields[0]);
            ((TextView) v.findViewById(R.id.customers_listview_field2)).setText(fields[1]);
            ((TextView) v.findViewById(R.id.customers_listview_field3)).setText(fields[2]);
            ((TextView) v.findViewById(R.id.customers_listview_field4)).setText(fields[3]);
            ((TextView) v.findViewById(R.id.customers_listview_field5)).setText(fields[4]);
            ((TextView) v.findViewById(R.id.customers_listview_field6)).setText(fields[5]);
            ((TextView) v.findViewById(R.id.customers_listview_field7)).setText(fields[6]);

            ((TextView) v.findViewById(R.id.customers_listview_field2)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent();
                    callIntent.setAction(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:"+fields[1]));

                    /* Verifico esista un'app in grado di gestire questo intent */
                    if (callIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        activity.startActivity(callIntent);
                    }
                }
            });


        }

        /* Setto un listener sulla email. L'email è sempre presente per cui non ho bisogno di alcun controllo */
        ((TextView) v.findViewById(R.id.customers_listview_field5)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");

                /*creo array di stringhe che contiene tutti gli indirizzi email a cui mandare l'email*/
                String[] addrTo = new String[1];
                addrTo[0] = fields[4];

                /*aggiungiamo dei dati all'intent*/
                intent.putExtra(Intent.EXTRA_EMAIL, addrTo);

                /*controllo se c'è una app capace di gestire l'intent e lo lancio*/
                if (intent.resolveActivity(getContext().getPackageManager())!=null) {
                    activity.startActivity(intent);
                }
            }
        });


        /* Al click del pulsante di delete, richiamo nella CustomersActivity il metodo per cancellare il cliente, passandogli la
         * sua posizione nella listview e l'email da cancellare */
        ((TextView) v.findViewById(R.id.customers_listview_delete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.delete_customer(position, fields[4]);
            }
        });


        if (expandedPosition == position) {
            /* Se la posizione da espandere coincide con quella dell'item cliccato, aumento l'altezza dell'item corrispondente */
            h = (int) getContext().getResources().getDimension(R.dimen.customer_list_item_height_extended);
            cl = v.findViewById(R.id.customers_listview_layout);
            lp = cl.getLayoutParams();
            lp.height = h;
            cl.setLayoutParams(lp);

            ((TextView) v.findViewById(R.id.customers_listview_field1)).setTextIsSelectable(true);
            ((TextView) v.findViewById(R.id.customers_listview_field2)).setTextIsSelectable(true);
            ((TextView) v.findViewById(R.id.customers_listview_field3)).setTextIsSelectable(true);
            ((TextView) v.findViewById(R.id.customers_listview_field4)).setTextIsSelectable(true);
            ((TextView) v.findViewById(R.id.customers_listview_field5)).setTextIsSelectable(true);
            ((TextView) v.findViewById(R.id.customers_listview_field6)).setTextIsSelectable(true);
            ((TextView) v.findViewById(R.id.customers_listview_field7)).setTextIsSelectable(true);


        } else {
            h = (int) getContext().getResources().getDimension(R.dimen.customer_list_item_height_shrinked);
            cl = v.findViewById(R.id.customers_listview_layout);
            lp = cl.getLayoutParams();
            lp.height = h;
            cl.setLayoutParams(lp);

            ((TextView) v.findViewById(R.id.customers_listview_field1)).setTextIsSelectable(false);
            ((TextView) v.findViewById(R.id.customers_listview_field2)).setTextIsSelectable(false);
            ((TextView) v.findViewById(R.id.customers_listview_field3)).setTextIsSelectable(false);
            ((TextView) v.findViewById(R.id.customers_listview_field4)).setTextIsSelectable(false);
            ((TextView) v.findViewById(R.id.customers_listview_field5)).setTextIsSelectable(false);
            ((TextView) v.findViewById(R.id.customers_listview_field6)).setTextIsSelectable(false);
            ((TextView) v.findViewById(R.id.customers_listview_field7)).setTextIsSelectable(false);
        }

        return v;
    }

    public void setExpandedItem(int position) {
        /* Aggiorno il valore di expandedPosition che indica quale elemento nella listView deve essere espanso */
        expandedPosition = position;
        notifyDataSetChanged();
    }

}
