package com.example.fabio.signin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;


public class CartFragment extends Fragment {
    private final String RECEIVER_EMAIL = "fabio.pipitone93@gmail.com";

    private ArrayList productList;
    private int[] orderResume;
    private ArrayList<Product> shrinkedList = new ArrayList<Product>();
    private TextView total_price;
    private OnFragmentInteractionListener mListener;

    public CartFragment() {
        // Required empty public constructor
    }

    public static CartFragment newInstance(ArrayList<Product> arrayList, int[] orderResume) {
        CartFragment fragment = new CartFragment();

        fragment.productList = arrayList;
        fragment.orderResume = orderResume;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /* Creo una lista dei prodotti che sono effettivamente nel carrello (e hanno quindi l'id corrispondente in orderResume
         * diverso da 0) */

        if (productList == null) productList = new ArrayList<Product>();
        shrinkArrayList(productList, orderResume);

        View root = inflater.inflate(R.layout.fragment_cart, container, false);
        final CartAdapter adapter = new CartAdapter(getContext(), shrinkedList, orderResume, this);
        ListView lv = root.findViewById(R.id.cart_listview_list);
        lv.setAdapter(adapter);

        total_price = root.findViewById(R.id.cart_total_price);
        computeTotalPrice();

        Button send_order = root.findViewById(R.id.cart_confirm_order);
        send_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setMessage(getResources().getString(R.string.send_order_confirm))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_SEND);
                                intent.setType("text/plain");

                                /*creo array di stringhe che contiene tutti gli indirizzi email a cui mandare l'email*/
                                String[] addrTo = new String[1];
                                addrTo[0] = RECEIVER_EMAIL;

                                /* Recupero i dati dell'utente dalle preferenze per inserirli nell'oggetto della email*/
                                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
                                String name = sharedPref.getString("name", null);
                                String surname = sharedPref.getString("surname", null);
                                String company = sharedPref.getString("company", null);
                                String email_object_start = getResources().getString(R.string.email_to_send_object);

                                /*aggiungiamo dei dati all'intent*/
                                intent.putExtra(Intent.EXTRA_EMAIL, addrTo);
                                intent.putExtra(Intent.EXTRA_SUBJECT, email_object_start+" "+name+" "+surname+" - "+company);
                                intent.putExtra(Intent.EXTRA_TEXT, getEmailText());

                                /*controllo se c'è una app capace di gestire l'intent e lo lancio*/
                                if (intent.resolveActivity(getContext().getPackageManager())!=null) {
                                    startActivity(intent);
                                    clearCart(adapter);
                                } else {
                                    Toast.makeText(getContext(), getString(R.string.no_email_app), Toast.LENGTH_SHORT).show();
                                }
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });

        ImageButton empty_cart_button = root.findViewById(R.id.cart_empty_cart);
        empty_cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearCart(adapter);
            }
        });

        return root;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(int id);
    }

    public void delFromCart(int id) {
        if (mListener != null) {
            mListener.onFragmentInteraction(id);
        }
    }

    public void computeTotalPrice() {
        float total = 0;
        int id = 0;
        for (int i = 0; i < shrinkedList.size(); i++) {

            id = shrinkedList.get(i).getId();
            total += shrinkedList.get(i).getPrice()*orderResume[id];

        }

        total_price.setText(getResources().getString(R.string.total_string)+" "+String.format("%.2f",total)+"€");
    }


    public ArrayList<Product> shrinkArrayList(ArrayList<Product> wholeList, int[] orderResume) {
        int id;

        for (int i = 0; i < wholeList.size(); i++) {

            id = wholeList.get(i).getId();
            if (orderResume[id] != 0) {
                shrinkedList.add(wholeList.get(i));
            }

        }

        return shrinkedList;
    }

    public String getEmailText() {
        String emailText = "";
        String pcs_abbrev = getResources().getString(R.string.cart_pieces_abbreviation);
        int product_id = 0;
        float subtotal = 0;
        float total = 0;
        float price_tmp;
        for (int i = 0; i < shrinkedList.size(); i++) {

            price_tmp = shrinkedList.get(i).getPrice();
            product_id = shrinkedList.get(i).getId();
            subtotal = orderResume[product_id] * price_tmp;
            emailText += shrinkedList.get(i).getName() + "  \t";
            emailText += String.format("%.2f",price_tmp) + "€/"+pcs_abbrev+"  \t";
            emailText += orderResume[product_id] + pcs_abbrev + "  \t";
            emailText += String.format("%.2f",subtotal) + "€\n\n";
            total += subtotal;

        }
        emailText += "\n  \t"+getResources().getString(R.string.total_string)+"  "+String.format("%.2f", total)+" €";

        return emailText;
    }

    public void clearCart(CartAdapter adapter) {
        for (int i = 0; i < orderResume.length; i++) {
            orderResume[i] = 0;
        }
        shrinkedList.clear();
        adapter.notifyDataSetChanged();
        computeTotalPrice();
    }
}
