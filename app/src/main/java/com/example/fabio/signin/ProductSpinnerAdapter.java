package com.example.fabio.signin;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ProductSpinnerAdapter extends ArrayAdapter {

    String[] values;

    public ProductSpinnerAdapter(@NonNull Context context, String[] objects) {
        super(context, R.layout.spinner_selected, objects);
        this.values = objects;

        setDropDownViewResource(R.layout.spinner_dropdown);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View v;

        if (convertView != null){
           v = convertView;
        }else {
            LayoutInflater li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.spinner_selected, parent, false);
        }

        TextView tv = v.findViewById(R.id.product_quantity);
        tv.setText(values[position]);
        return v;
    }
}
