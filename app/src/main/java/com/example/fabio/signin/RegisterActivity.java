package com.example.fabio.signin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RegisterActivity extends AppCompatActivity {

    /* Messaggi di ritorno dalla servlet */
    private final String EMAIL_ERROR = "EMAIL NOT FOUND";
    private final String CUSTOMER_ERROR = "CUSTOMER ALREADY REGISTERED";
    private final String USERNAME_ERROR = "USERNAME ALREADY USED";
    private final String SUCCESSFUL_REGISTRATION = "CUSTOMER SUCCESSFULLY REGISTERED";

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(){ //creo handler per ricevere messaggi dal thread
        @Override
        public void handleMessage(Message msg) {

            String threadResponse = msg.getData().getString(ServletThread.SERVLET_THREAD_LABEL);
            if (threadResponse == null) {
                Toast.makeText(getApplicationContext(), getString(R.string.connection_problems), Toast.LENGTH_SHORT).show();
            } else {
                try {
                    JSONObject jobj = new JSONObject(threadResponse);
                    String result = jobj.getString("result");
                    if (result.equals(EMAIL_ERROR)) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.register_email_not_approved), Toast.LENGTH_SHORT).show();
                    } else if (result.equals(CUSTOMER_ERROR)) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.register_client_already_registered), Toast.LENGTH_SHORT).show();
                    }else if (result.equals(USERNAME_ERROR)) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.register_username_already_used), Toast.LENGTH_SHORT).show();
                    } else if (result.equals(SUCCESSFUL_REGISTRATION)) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.register_successful_registration), Toast.LENGTH_SHORT).show();
                        Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                        RegisterActivity.this.startActivity(loginIntent);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        Button reg_button = (Button) findViewById(R.id.button_register);


        reg_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ServletThread(ServletThread.REGISTER, servletDataBuilder(), handler).start();
            }
        });

    }

    public String servletDataBuilder(){

        EditText name = (EditText) findViewById(R.id.name_field);
        EditText surname = (EditText) findViewById(R.id.surname_field);
        EditText company = (EditText) findViewById(R.id.company_field);
        EditText piva = (EditText) findViewById(R.id.piva_field);
        EditText phone = (EditText) findViewById(R.id.phone_field);
        EditText email = (EditText) findViewById(R.id.email_field);
        EditText username = (EditText) findViewById(R.id.username_field);
        EditText pw = (EditText) findViewById(R.id.password_field);
        EditText address = (EditText) findViewById(R.id.address_field);

        /* Faccio l'hash della password inserita dall'utente */
        MessageDigest md = null;
        byte[] digest = null;
        StringBuilder hashed_pw = new StringBuilder();
        try {
            md = MessageDigest.getInstance("SHA-512");
            digest = md.digest(pw.getText().toString().getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < digest.length; i++) {
            hashed_pw.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
        }


        JSONObject jobj = new JSONObject();
        try {
            jobj.put("name", name.getText().toString());
            jobj.put("surname", surname.getText().toString());
            jobj.put("company", company.getText().toString());
            jobj.put("piva", piva.getText().toString());
            jobj.put("phone", phone.getText().toString());
            jobj.put("email", email.getText().toString());
            jobj.put("username", username.getText().toString());
            jobj.put("password", hashed_pw.toString());
            jobj.put("address", address.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jobj.toString();
    }
}
