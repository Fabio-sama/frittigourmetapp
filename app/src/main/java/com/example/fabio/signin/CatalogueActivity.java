package com.example.fabio.signin;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class CatalogueActivity extends AppCompatActivity implements CategoryFragment.OnFragmentInteractionListener, CartFragment.OnFragmentInteractionListener {

    private ArrayList<Product> productsList = new ArrayList<Product>();
    private ArrayList<Product> cat1List = new ArrayList<Product>();
    private ArrayList<Product> cat2List = new ArrayList<Product>();
    private ArrayList<Product> cat3List = new ArrayList<Product>();
    private ArrayList<Product> cat4List = new ArrayList<Product>();
    private final String CATEGORY1 = "PANATI";
    private final String CATEGORY2 = "PASTELLATI";
    private final String CATEGORY3 = "FINGERFOOD";
    private final String CATEGORY4 = "PERSONALIZZATI";
    private final String CACHE_FILE = "catalogue";
    static final String CATALOGUE_FRAG = "CATALOGUE";
    private final String ACTION_WRITE = "action_write";
    private final String ACTION_READ = "action_read";
    private final long DELAY = 86400000; //millisecondi in un giorno
    static final String FRAG_TO_SET = "fragment_to_set";
    static final String CART_FRAG = "CART";
    private int[] orderResume = new int[1];
    private ImageView[] category_pics = new ImageView[4];
    private TextView catalogue_category_name;
    private ImageView upper_bar_catalogue;
    private ImageView upper_bar_cart;
    private ImageView catalogue_plus_one;
    private boolean is_admin;



    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(){ //creo handler per ricevere messaggi dal thread
        @Override
        public void handleMessage(Message msg) {

            String threadResponse = msg.getData().getString(ServletThread.SERVLET_THREAD_LABEL);

            if (threadResponse == null) {
                Toast.makeText(getApplicationContext(), getString(R.string.connection_problems), Toast.LENGTH_SHORT).show();

                /* Se non riesco a raggiungere il server, do la possibilità all'utente di caricare il catalogue precedentemente
                 * salvato nella cache */
                final Button conn_problem = findViewById(R.id.catalogue_connection_problems);
                conn_problem.setVisibility(View.VISIBLE);
                conn_problem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        if (sharedPref.getLong("last_downloaded_catalogue", 0) == 0) {
                            /* Se non ho mai acceduto al server, non avrò nessun catalogo in memoria */
                            Toast.makeText(CatalogueActivity.this, getResources().getString(R.string.catalogue_empty_cache), Toast.LENGTH_SHORT).show();
                        } else {
                            new RetrieveCatalogue().execute(ACTION_READ);
                            conn_problem.setVisibility(View.GONE);
                        }
                    }
                });
            } else {

                try {
                    /* Ci ritornerà un array di JSON con i vari prodotti. */
                    JSONArray jcontainer = new JSONArray(threadResponse);

                    setupCatalogue(jcontainer);

                } catch (Exception e){
                    e.printStackTrace();
                }

                /* Salviamo i dati scaricati dal server nella cache tramite la Asynctask */
                new RetrieveCatalogue().execute(ACTION_WRITE);

            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue);


        final ImageView upper_bar_account = findViewById(R.id.upper_bar_user);
        upper_bar_cart = findViewById(R.id.upper_bar_cart);
        upper_bar_catalogue = findViewById(R.id.upper_bar_catalogue);
        catalogue_category_name = findViewById(R.id.catalogue_category_name);

        is_admin = getIntent().getBooleanExtra("is_admin", false);


        /* Se sono l'amministratore oppure sono un utente ma ho richiesto il fragment del catalogo, illumino l'icona catalogo */
        if (getIntent().getStringExtra(FRAG_TO_SET).compareTo(CATALOGUE_FRAG) == 0 || is_admin) {
            upper_bar_cart.setBackground(null);
            upper_bar_catalogue.setColorFilter(getResources().getColor(R.color.upper_bar_icon_selected));
            upper_bar_catalogue.setBackgroundResource(R.drawable.icon_background);

        } else {
            /* Altrimenti illumino l'icona carrello */
            upper_bar_catalogue.setBackground(null);
            upper_bar_cart.setColorFilter(getResources().getColor(R.color.upper_bar_icon_selected));
            upper_bar_cart.setBackgroundResource(R.drawable.icon_background);
        }

        /* Se clicco sull'icona account devo discernere se sto cliccando da utente o da admin e far partire l'activity corrispondente */
        upper_bar_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i;
                if (is_admin) {
                    i = new Intent(CatalogueActivity.this, AccountAdminActivity.class);
                } else {
                    i = new Intent(CatalogueActivity.this, AccountActivity.class);
                }
                startActivity(i);
            }
        });

        /* Se sono l'admin anziché l'immagine del carrello devo settare quella della lista clienti */
        if (is_admin) upper_bar_cart.setImageResource(R.drawable.ic_action_customers);

        upper_bar_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_admin) {
                    /* Se sono l'admin lancio la CustomersActivity*/
                    Intent i = new Intent(CatalogueActivity.this, CustomersActivity.class);
                    startActivity(i);
                } else {
                    /* Se sono un cliente devo passare al fragment del carrello*/
                    upper_bar_catalogue.setBackground(null);
                    upper_bar_catalogue.setColorFilter(getResources().getColor(android.R.color.white));
                    upper_bar_cart.setColorFilter(getResources().getColor(R.color.upper_bar_icon_selected));
                    upper_bar_cart.setBackgroundResource(R.drawable.icon_background);

                    /* Se sto nel carrello, tutte le thumbs delle categorie devono essere opache */
                    setAlpha(-1);

                    CartFragment fp = CartFragment.newInstance(productsList, orderResume);
                    FragmentManager fm = getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.catalogue_fragment_container, fp);
                    ft.commit();

                    /* Aggiorno il titolo del fragment sostituendo il nome della categoria con la scritta di riepilogo */
                    String resume = getApplicationContext().getResources().getString(R.string.cart_order_resume);
                    catalogue_category_name.setText(resume);

                    /* imposto l'intent per non avere problemi di aggiornamento nella rotazione dello schermo */
                    getIntent().putExtra(FRAG_TO_SET, CART_FRAG);
                }
            }
        });

        if (!is_admin) {
            /* Solo se non sono admin ha senso mettere un onClickListener sull'icona catalogue */
            upper_bar_catalogue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    upper_bar_cart.setBackground(null);
                    upper_bar_cart.setColorFilter(getResources().getColor(android.R.color.white));
                    upper_bar_catalogue.setColorFilter(getResources().getColor(R.color.upper_bar_icon_selected));
                    upper_bar_catalogue.setBackgroundResource(R.drawable.icon_background);

                    /* Cliccando sul catalogo di default vado sul fragment della prima categoria, quindi devo illuminarne la thumb */
                    setAlpha(0);

                    CategoryFragment fp = CategoryFragment.newInstance(cat1List);
                    FragmentManager fm = getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.catalogue_fragment_container, fp);
                    ft.commit();
                    catalogue_category_name.setText(getResources().getString(R.string.category1_name));

                    getIntent().putExtra(FRAG_TO_SET, CATALOGUE_FRAG);
                }
            });
        }

        /* Decido se lanciare un nuovo thread per richiedere la lista prodotti al server o se recuperarla dalla cache */

        if (checkExpiringDate()) {
            /* Devo scaricare il catalogo dal server */

            new ServletThread(ServletThread.PRODUCTS_LIST, "", handler).start();
        } else {
            /* Lancio una AsyncTask per recuperare i dati dalla cache */

            new RetrieveCatalogue().execute(ACTION_READ);
        }
    }



    @Override
    protected void onPause() {
        super.onPause();

        if (!is_admin) {
            /* Solo se non sono admin ha senso salvare i dati non ancora inviati tra le preferenze */
            saveOrderResume();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void extractCategories() {
        Product prod;
        String category;
        int n = productsList.size();


        for (int i = 0; i < n; i++) {
            prod = productsList.get(i);

            category = prod.getCategory();

            if (category.compareTo(CATEGORY1) == 0) {
                cat1List.add(prod);
            } else if (category.compareTo(CATEGORY2) == 0) {
                cat2List.add(prod);
            } else if (category.compareTo(CATEGORY3) == 0) {
                cat3List.add(prod);
            } else {
                cat4List.add(prod);
            }
        }
    }


    @Override
    public void onFragmentInteraction(int id, int quantity) {
        orderResume[id] += quantity;

        final int time_duration = 500;

        catalogue_plus_one = findViewById(R.id.catalogue_plus_one);

        catalogue_plus_one.animate()
                .alpha(1.0f)
                .setDuration(time_duration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        catalogue_plus_one.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        catalogue_plus_one.animate()
                                .alpha(0.0f)
                                .setDuration(time_duration)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationCancel(animation);
                                        catalogue_plus_one.setVisibility(View.GONE);

                                    }
                                });
                    }
                });
    }

    public void retrieveOrderResume() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String orderResumeString = sharedPref.getString("orderResumeString", null);

        int len = 0;

        /* Prendo l'id più grande nella lista prodotti. Creerò un array di interi grande quanto questo valore.
         * L'indice di ogni elemento nell'array corrisponderà all'id di un prodotto e il valore sarà la quantità corrispondente.
         * (l'array viene creato con una posizione in più per avere esatta corrispondenza tra id e indice)*/
        len = findLenght(productsList);


        if (orderResumeString == null) {
            /* Non ho dati salvati nel carrello, quindi è la prima volta che apro l'app */
            orderResume = new int[len];
        } else {
            orderResume = stringToIntArray(orderResumeString);
            if (orderResume.length != len) {
                /* Se la lunghezza dell'array nelle preferenze è diversa da quella ricavata dal DB, creo un nuovo intArray della
                 * lunghezza corretta con gli spazi aggiunti lasciati a valore 0. */
                int[] newArray = new int[len];
                for (int i = 0; i < orderResume.length; i++) {
                    newArray[i] = orderResume[i];
                }
                /* Assegno alla variabile globale orderResume il nuovo array  e lo salvo nelle preferenze*/
                orderResume = newArray;
                saveOrderResume();
            }
        }
    }

    public int findLenght(ArrayList<Product> list) {
        int max_id = -1;

        for (int i = 0; i < list.size(); i++){

            int id = list.get(i).getId();
            if (id > max_id) max_id = id;
        }

        return max_id + 1;
    }


    public void saveOrderResume() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        String orderResumeString = intArrayToString(orderResume);
        editor.putString("orderResumeString", orderResumeString);
        editor.commit();
    }


    public int[] stringToIntArray(String arrayStr) {
        String[] strArray = arrayStr.split(" ");
        int[] arrayInt = new int[strArray.length];

        for(int i = 0; i < strArray.length; i++) {
            arrayInt[i] = Integer.parseInt(strArray[i]);
        }

        return arrayInt;
    }

    public String intArrayToString(int[] arrayInt) {
        String arrayStr = "";

        arrayStr += arrayInt[0];
        for (int i = 1; i < arrayInt.length; i++) {
            arrayStr += " "+arrayInt[i];
        }

        return arrayStr;
    }

    @Override
    public void onFragmentInteraction(int id) {
        orderResume[id] = 0;
    }

    @Override
    public boolean onFragmentInteraction() {
        return is_admin;
    }

    public void setAlpha(int category_id) {
        for (int i = 0; i < 4; i++) {
            category_pics[i].setAlpha(0.5f);
        }
        /* Se sono nel carrello avrò impostato alpha a -1 perché tutte le thumbs delle categorie devono essere opache */
        if (category_id != -1) category_pics[category_id].setAlpha(1f);
    }

    public boolean checkExpiringDate() {
        /* La funzione ritorna true nel caso in cui è necessario recuperare le informazioni dal server, false altrimenti. */
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        long oldTimestamp = sharedPref.getLong("last_downloaded_catalogue", 0);
        long newTimestamp;

        if (oldTimestamp == 0) {
            return true;
        } else {
            newTimestamp = System.currentTimeMillis();
            if (newTimestamp-oldTimestamp > DELAY) {
                return true;
            }
            return false;
        }
    }

    public void setExpiringDate() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();

        long currentTimestamp = System.currentTimeMillis();
        editor.putLong("last_downloaded_catalogue", currentTimestamp);
        editor.commit();
    }

    public void setupCatalogue(JSONArray jcontainer) throws JSONException{

        productsList = new ArrayList<Product>();

        /* Creo un arraylist con gli oggetti JSON */
        for (int i=0; i<jcontainer.length(); i++) {
                Product prod = new Product(jcontainer.getJSONObject(i));
                productsList.add(prod);
        }

        /* Creo le 4 sottoliste per le differenti categorie di prodotti */
        extractCategories();

        /* Recupero dalle preferenze i dati del carrello */
        if (!is_admin) retrieveOrderResume();


        /* Prendo i riferimenti alle 4 image view delle categorie e imposto sul click di ognuna la visualizzazione
         * di un determinato fragment */
        category_pics[0] = findViewById(R.id.catalogue_cat1);
        category_pics[1] = findViewById(R.id.catalogue_cat2);
        category_pics[2] = findViewById(R.id.catalogue_cat3);
        category_pics[3] = findViewById(R.id.catalogue_cat4);

        /* Inizializzo il primo fragment a seconda che dal pannello utente sia stato cliccato il pulsante catalogo
         * oppure carrello*/
        if (getIntent().getStringExtra(FRAG_TO_SET).compareTo(CATALOGUE_FRAG) == 0 || is_admin) {
            setAlpha(0);
            CategoryFragment fp = CategoryFragment.newInstance(cat1List);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.catalogue_fragment_container, fp);
            ft.commit();
            catalogue_category_name.setText(getResources().getString(R.string.category1_name));
        } else {
            CartFragment fp = CartFragment.newInstance(productsList, orderResume);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.catalogue_fragment_container, fp);
            ft.commit();
            String resume = getApplicationContext().getResources().getString(R.string.cart_order_resume);
            catalogue_category_name.setText(resume);
        }


        category_pics[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAlpha(0);

                /* Sto passando al fragment del catalogo e quindi opacizzo il toggle del carrello */
                upper_bar_cart.setBackground(null);
                upper_bar_cart.setColorFilter(getResources().getColor(android.R.color.white));
                upper_bar_catalogue.setColorFilter(getResources().getColor(R.color.upper_bar_icon_selected));
                upper_bar_catalogue.setBackgroundResource(R.drawable.icon_background);

                CategoryFragment fp = CategoryFragment.newInstance(cat1List);
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.catalogue_fragment_container, fp);
                ft.commit();
                catalogue_category_name.setText(getResources().getString(R.string.category1_name));
            }
        });

        category_pics[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAlpha(1);

                /* Sto passando al fragment del catalogo e quindi opacizzo il toggle del carrello */
                upper_bar_cart.setBackground(null);
                upper_bar_cart.setColorFilter(getResources().getColor(android.R.color.white));
                upper_bar_catalogue.setColorFilter(getResources().getColor(R.color.upper_bar_icon_selected));
                upper_bar_catalogue.setBackgroundResource(R.drawable.icon_background);

                CategoryFragment fp = CategoryFragment.newInstance(cat2List);
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.catalogue_fragment_container, fp);
                ft.commit();
                catalogue_category_name.setText(getResources().getString(R.string.category2_name));
            }
        });

        category_pics[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAlpha(2);

                /* Sto passando al fragment del catalogo e quindi opacizzo il toggle del carrello */
                upper_bar_cart.setBackground(null);
                upper_bar_cart.setColorFilter(getResources().getColor(android.R.color.white));
                upper_bar_catalogue.setColorFilter(getResources().getColor(R.color.upper_bar_icon_selected));
                upper_bar_catalogue.setBackgroundResource(R.drawable.icon_background);

                CategoryFragment fp = CategoryFragment.newInstance(cat3List);
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.catalogue_fragment_container, fp);
                ft.commit();
                catalogue_category_name.setText(getResources().getString(R.string.category3_name));
            }
        });

        category_pics[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAlpha(3);

                /* Sto passando al fragment del catalogo e quindi opacizzo il toggle del carrello */
                upper_bar_cart.setBackground(null);
                upper_bar_cart.setColorFilter(getResources().getColor(android.R.color.white));
                upper_bar_catalogue.setColorFilter(getResources().getColor(R.color.upper_bar_icon_selected));
                upper_bar_catalogue.setBackgroundResource(R.drawable.icon_background);

                CategoryFragment fp = CategoryFragment.newInstance(cat4List);
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.catalogue_fragment_container, fp);
                ft.commit();
                catalogue_category_name.setText(getResources().getString(R.string.category4_name));
            }
        });
    }

    private class RetrieveCatalogue extends AsyncTask<String, Void, JSONArray> {

        @Override
        protected JSONArray doInBackground(String... strings) {
            File cacheDirectory = getCacheDir();

            /* Vedo l'azione che dovrò fare con questa AsyncTask, se leggere dalla cache o scriverci */
            String command = strings[0];

            /* Creo il file */
            File filename = new File(cacheDirectory, CACHE_FILE);

            if (command.compareTo(ACTION_WRITE) == 0) {
                /* Salvo in cache il catalogo prodotti e ritorno null così da non procedere nella onPostExecute */

                /* Salvo i JSON nella ArrayList productsList in un JSONArray che uso nella AsyncTask */
                JSONArray jarr = new JSONArray();
                try {
                    for (int i = 0; i < productsList.size(); i++) {
                        jarr.put(productsList.get(i).toJSON());
                    }
                    BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
                    writer.write(jarr.toString());
                    writer.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                /* Salvo nelle preferenze il timestamp corrente */
                setExpiringDate();

                return null;

            } else {
                /* Leggo il catalogo salvato in cache */

                if (!filename.exists()) {
                    /* Superfluo perché se mi trovo qui devo aver creato il file */
                    return null;
                }

                /* Se il file esiste */
                String content = "";
                JSONArray jarr = null;
                try {
                    FileInputStream fis = new FileInputStream(filename);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
                    String line = reader.readLine();
                    while (line != null) {
                        content += line;
                        line = reader.readLine();
                    }
                    reader.close();
                    fis.close();

                    jarr = new JSONArray(content);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return jarr;
            }




        }

        @Override
        protected void onPostExecute(JSONArray jArray) {

            if (jArray == null) {
                return;
            }

            try {
                setupCatalogue(jArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
