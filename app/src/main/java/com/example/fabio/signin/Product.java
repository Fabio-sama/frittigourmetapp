package com.example.fabio.signin;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Product {

    private int id;
    private String category;
    private String name;
    private Float price;

    public Product(JSONObject jobj) {
        try {
            this.name = jobj.getString("product_name");
            this.category = jobj.getString("product_category");
            this.id = Integer.parseInt(jobj.getString("product_id"));
            this.price = Float.parseFloat(jobj.getString("product_price"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public JSONObject toJSON () {
        JSONObject jobj = new JSONObject();

        try {
            jobj.put("product_id", getId() + "");
            jobj.put("product_name", getName());
            jobj.put("product_category", getCategory());
            jobj.put("product_price", getPrice() + "");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jobj;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }




}
