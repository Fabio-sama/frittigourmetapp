package com.example.fabio.signin;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class CartAdapter extends ArrayAdapter {

    private ArrayList<Product> products;
    private int[] orderResume;
    private CartFragment fragment;

    public CartAdapter(@NonNull Context context, ArrayList<Product> objects, int[] orderResume, CartFragment fragment) {
        super(context, android.R.layout.simple_list_item_1, objects);
        products = objects;
        this.orderResume = orderResume;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View root;

        if (convertView != null) {
            root = convertView;
        } else {
            LayoutInflater li = LayoutInflater.from(getContext());
            root = li.inflate(R.layout.cart_listview_item, parent, false);
        }

        TextView name = root.findViewById(R.id.cart_listview_name);
        TextView single_price_tv = root.findViewById(R.id.cart_listview_single_price);
        TextView subtotal_price = root.findViewById(R.id.cart_listview_subtotal_price);
        TextView pieces = root.findViewById(R.id.cart_listview_pieces);
        ImageView clear = root.findViewById(R.id.cart_listview_clear);
        ConstraintLayout layout = root.findViewById(R.id.cart_listview_layout);



        if (position%2 == 0) {
            layout.setBackgroundResource(R.color.cart_listview_light);
        } else {
            layout.setBackgroundResource(R.color.cart_listview_dark);
        }



        float subtotal = 0;
        int product_id = products.get(position).getId();
        String pcs = getContext().getResources().getString(R.string.cart_pieces_abbreviation);
        float single_price = products.get(position).getPrice();
        name.setText(products.get(position).getName());
        single_price_tv.setText(String.format("%.2f",single_price)+" €/"+pcs);
        subtotal = single_price*orderResume[product_id];
        subtotal_price.setText(String.format("%.2f",subtotal)+"€");
        pieces.setText(orderResume[product_id]+" "+pcs);


        /* Aggiungo un listener per eliminare un prodotto dal carrello */
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int product_id = 0;

                product_id = products.get(position).getId();

                fragment.delFromCart(product_id);
                products.remove(position);
                notifyDataSetChanged();
                fragment.computeTotalPrice();
            }
        });

        return root;
    }



}
