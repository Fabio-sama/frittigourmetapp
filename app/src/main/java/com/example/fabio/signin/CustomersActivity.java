package com.example.fabio.signin;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CustomersActivity extends AppCompatActivity {

    private ArrayList<JSONObject> customersList;
    private CustomersAdapter adapter;
    private final String RESULT_SHOW = "OK_SHOW";
    private final String RESULT_DELETE = "OK_DELETE";
    private final String RESULT_ADD = "OK_ADD";
    private final String RESULT_NOADD = "NO_ADD";
    private final String ACTION_SHOW = "action_show";
    private final String ACTION_ADD = "action_add";
    private final String ACTION_DELETE = "action_delete";
    private JSONObject user_data_json;
    private EditText target_email;
    private int target_position;

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(){ //creo handler per ricevere messaggi dal thread
        @Override
        public void handleMessage(Message msg) {

            String threadResponse = msg.getData().getString(ServletThread.SERVLET_THREAD_LABEL);


            if (threadResponse == null) {
                Toast.makeText(getApplicationContext(), getString(R.string.connection_problems), Toast.LENGTH_SHORT).show();
            } else {

                try {
                    /* Ci ritornerà un array di JSON. Dobbiamo distinguere i tre casi: SHOW, ADD, DELETE */
                    JSONArray jcontainer = new JSONArray(threadResponse);
                    String query_result = jcontainer.getJSONObject(jcontainer.length()-1).getString("result");

                    if (query_result.compareTo(RESULT_SHOW) == 0) {
                        /* Come ultimo elemento dell'array ho ["result":"OK_SHOW"] quindi mostro la lista clienti */
                        if (jcontainer.length() == 0) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_empty_client_list), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        /* Creo un arraylist con gli oggetti JSON */
                        customersList = new ArrayList<JSONObject>();
                        for (int i=0; i<jcontainer.length()-1; i++) {
                            customersList.add(jcontainer.getJSONObject(i));
                        }

                        /* Creo l'adapter e gli assegno l'arraylist creato */
                        adapter = new CustomersAdapter(getApplicationContext(), customersList, CustomersActivity.this);
                        ListView lv = findViewById(R.id.customers_listview);
                        lv.setAdapter(adapter);

                        /* Voglio che cliccando un item nella listView, questo si espanda mostrando le altre info sul cliente.
                        Aggiungo quindi un listener sul click dell'elemento per espandere l'item cliccato e far collassare gli altri */
                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                ((CustomersAdapter) adapter).setExpandedItem(position);
                            }
                        });

                    } else if (query_result.compareTo(RESULT_DELETE) == 0) {
                        /* Come ultimo elemento dell'array ho ["result":"OK_DELETE"]. Se sono qui ho già cancellato l'utente nel DB
                         * e devo solo aggiornare la grafica rimuovendo la riga corrispondente */
                        customersList.remove(target_position);
                        adapter.notifyDataSetChanged();
                        Toast.makeText(CustomersActivity.this, getResources().getString(R.string.user_removed_from_db), Toast.LENGTH_SHORT).show();

                    } else if (query_result.compareTo(RESULT_ADD) == 0) {
                        /* L'email è valida. Ho già aggiunto l'utente al DB e quindi devo aggiungerlo in grafica */
                        JSONObject jobj = new JSONObject();
                        String unknown = getResources().getString(R.string.unknown_customers_field);
                        jobj.put("username",unknown);
                        jobj.put("name",unknown);
                        jobj.put("surname",unknown);
                        jobj.put("company",unknown);
                        jobj.put("piva",unknown);
                        jobj.put("phone",unknown);
                        jobj.put("email",target_email.getText().toString().trim());
                        jobj.put("address",unknown);

                        target_email.setText("");

                        customersList.add(0, jobj);
                        adapter.notifyDataSetChanged();
                        Toast.makeText(CustomersActivity.this, getResources().getString(R.string.user_preapproved), Toast.LENGTH_SHORT).show();

                    } else if (query_result.compareTo(RESULT_NOADD) == 0) {
                        /* Un utente con l'email da aggiungere è già presente nel DB */
                        Toast.makeText(CustomersActivity.this, getResources().getString(R.string.user_not_added), Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers);

        /* jobj non può essere nullo perché devono esserci dei dati salvati nelle preferenze perché l'admin deve essere già loggato
         * per accedere a questa activity */
        user_data_json = retrieveData();
        target_email = findViewById(R.id.customers_target_email);

        /* Lancio il thread per creare la lista clienti. Il secondo parametro sarà null perché non mi serve una target_email
         * specifica */
        new ServletThread(ServletThread.CUSTOMERS_LIST, servletDataBuilder(ACTION_SHOW, null), handler).start();

        /* Pulsante per aggiungere un utente al DB */
        ImageButton b_add = findViewById(R.id.customers_add_user);
        b_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_customer();
            }
        });

        ImageView admin_account = findViewById(R.id.upper_bar_user);
        admin_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CustomersActivity.this, AccountAdminActivity.class);
                startActivity(i);
            }
        });

        ImageView admin_catalogue = findViewById(R.id.upper_bar_catalogue);
        admin_catalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CustomersActivity.this, CatalogueActivity.class);
                i.putExtra(CatalogueActivity.FRAG_TO_SET, CatalogueActivity.CATALOGUE_FRAG);
                i.putExtra("is_admin", true);
                startActivity(i);
            }
        });
    }

    public String servletDataBuilder(String action, String target){

        JSONObject jobj = new JSONObject();
        try {
            jobj.put("username", user_data_json.getString("username"));
            jobj.put("password", user_data_json.getString("password"));
            jobj.put("action", action);

            if (target != null) jobj.put("target_email", target);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jobj.toString();
    }




    public JSONObject retrieveData() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        JSONObject jobj = new JSONObject();

        try {
            jobj.put("username", sharedPref.getString("username", null));
            jobj.put("password", sharedPref.getString("password", null));
            if (jobj.getString("username") == null) {
                return null;
            } else {
                return jobj;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void delete_customer(final int position, final String emailToDelete) {
        /* Funzione che lancia il thread per cancellare il cliente nel DB e tiene in memoria la posizione relativa */

        /* Mostriamo il messaggio di conferma prima di cancellare il cliente */
            new AlertDialog.Builder(this)
                    .setMessage(getResources().getString(R.string.delete_customer_confirm))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            new ServletThread(ServletThread.CUSTOMERS_LIST, servletDataBuilder(ACTION_DELETE, emailToDelete), handler).start();

                            target_position = position;
                        }})
                    .setNegativeButton(android.R.string.no, null).show();
    }




    public void add_customer() {
        String target_email_string = target_email.getText().toString().trim();
        if (target_email_string.compareTo("") == 0 || target_email_string.indexOf('@') < 0 || target_email_string.indexOf('.') <= 0) {
            Toast.makeText(this, getResources().getString(R.string.invalid_email_error), Toast.LENGTH_SHORT).show();
        } else {
            new ServletThread(ServletThread.CUSTOMERS_LIST, servletDataBuilder(ACTION_ADD, target_email_string), handler).start();
        }
    }
}
