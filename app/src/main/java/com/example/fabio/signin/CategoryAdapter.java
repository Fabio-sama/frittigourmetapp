package com.example.fabio.signin;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoryAdapter extends ArrayAdapter {

    private ArrayList<Product> products;
    private CategoryFragment fragment;
    private boolean is_admin;


    public CategoryAdapter(@NonNull Context context, ArrayList<Product> objects, CategoryFragment fragment) {
        super(context, android.R.layout.simple_list_item_1, objects);
        products = objects;
        this.fragment = fragment;
        this.is_admin = fragment.get_is_admin();
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View root;
        if (convertView != null) {
            root = convertView;
        } else {
            LayoutInflater li = LayoutInflater.from(getContext());
            root = li.inflate(R.layout.category_listview_item, parent, false);
        }

        ConstraintLayout layout = root.findViewById(R.id.category_product_layout);

        /* Alterno i colori delle righe nella listview */
        if (position%2 == 0) {
            layout.setBackgroundResource(R.color.cart_listview_light);
        } else {
            layout.setBackgroundResource(R.color.cart_listview_dark);
        }

        TextView name = root.findViewById(R.id.category_product_name);
        TextView price = root.findViewById(R.id.category_product_price);
        ImageView thumb = root.findViewById(R.id.category_product_pic);
        ImageButton addToCart = root.findViewById(R.id.category_product_add_to_cart);
        final Spinner spinner = root.findViewById(R.id.category_product_spinner);

        if (!is_admin) {
            /* Se non sono l'admin mi serve avere lo spinner per selezionare la quantità di prodotti*/
            String[] list = {"10", "20", "30", "40", "50"};
            ProductSpinnerAdapter spinnerAdapter = new ProductSpinnerAdapter(getContext(), list);
            spinner.setAdapter(spinnerAdapter);
        } else {
            /* Se sono l'admin tolgo lo spinner */
            spinner.setVisibility(View.GONE);
        }

        name.setText(products.get(position).getName());

        float price_tmp = products.get(position).getPrice();
        price.setText(String.format("%.2f",price_tmp)+" €");
        int id = getContext().getResources().getIdentifier("prod"+products.get(position).getId(), "drawable", getContext().getPackageName());
        thumb.setImageResource(id);


        if (!is_admin) {
            /* Se sono l'utente devo poter aggiungere i prodotti al carrello */
            addToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int product_id = 0;
                    product_id = products.get(position).getId();
                    int product_quantity = Integer.parseInt((String) spinner.getSelectedItem());

                    fragment.addToCart(product_id, product_quantity);
                }
            });
        } else {
            /* Se sono l'admin tolgo il pulsante per aggiungere al carrello */
            addToCart.setVisibility(View.GONE);
        }

        return root;
    }
}
