package com.example.fabio.signin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends AppCompatActivity {

    /* Messaggi che ritornano dalla servlet */
    final String WRONG_DATA = "WRONG USER OR PASSWORD";
    final String ADMIN_SUCCESS = "ADMIN SUCCESSFULLY LOGGED IN";
    final String USER_SUCCESS = "USER SUCCESSFULLY LOGGED IN";

    /*  */
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {

            String threadResponse = msg.getData().getString(ServletThread.SERVLET_THREAD_LABEL);

            if (threadResponse == null) {
                Toast.makeText(getApplicationContext(), getString(R.string.connection_problems), Toast.LENGTH_SHORT).show();
            } else {
                try {
                    /* Il thread mi invierà una stringa in formato JSON contenente i campi (name, surname, company, piva, phone, email, result) */
                    JSONObject jobj = new JSONObject(threadResponse);

                    /* Aggiungo al JSON anche i campi username e password che ricavo dalla schermata di Login*/
                    jobj.put("username", ((EditText) findViewById(R.id.login_username)).getText().toString());
                    jobj.put("password", ((EditText) findViewById(R.id.username_field)).getText().toString());

                    /* In base al risultato sclego cosa fare */
                    String result = jobj.getString("result");
                    if (result.equals(WRONG_DATA)) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.wrong_pw_or_user), Toast.LENGTH_SHORT).show();
                    } else if (result.equals(ADMIN_SUCCESS)) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_admin), Toast.LENGTH_SHORT).show();
                        jobj.put("admin", true);

                        /* Salviamo i dati in locale e avviamo l'activity per l'admin */
                        saveData(jobj);
                        Intent accountAdminIntent = new Intent(LoginActivity.this, AccountAdminActivity.class);
                        LoginActivity.this.startActivity(accountAdminIntent);
                    } else if (result.equals(USER_SUCCESS)) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.login_user), Toast.LENGTH_SHORT).show();
                        jobj.put("admin", false);

                        /* Salviamo i dati in locale e avviamo l'activity per l'user semplice */
                        saveData(jobj);
                        Intent accountIntent = new Intent(LoginActivity.this, AccountActivity.class);
                        LoginActivity.this.startActivity(accountIntent);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        JSONObject jobj = retrieveData();

        /* Se retrieveData non ci ha restituito null significa che ho i dati salvati nelle preferenze e quindi non devo fare il Login */
        if (jobj != null) {
            Intent accountIntent = null;
            try {
                if (jobj.getBoolean("admin")) {
                    /* Se i dati salvati sono quelli dell'amministratore, mostrerò la schermata relativa */
                    accountIntent = new Intent(LoginActivity.this, AccountAdminActivity.class);
                } else {
                    /* Altrimenti mostro la schermata dell'utente classico */
                    accountIntent = new Intent(LoginActivity.this, AccountActivity.class);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            LoginActivity.this.startActivity(accountIntent);
        }

        /* Se sono qui, significa che non ho dati salvati nelle preferenze. Dovrò quindi effettuare il Login o dare la possibilità
        * di registrarsi*/


        Button login_button = (Button) findViewById(R.id.button_login);
        TextView registerLink = (TextView) findViewById(R.id.register_here);

        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* Rimando all'activity per la registrazione */
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }
        });

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* Sul click del tasto di Login, avvio il thread per controllare i dati inseriti dall'utente */
                new ServletThread(ServletThread.LOGIN, servletDataBuilder(), handler).start();
            }
        });


    }

    public String servletDataBuilder(){
        EditText username = (EditText) findViewById(R.id.login_username);
        EditText pw = (EditText) findViewById(R.id.username_field);

        String hashed_pw = hashPassword(pw.getText().toString());

        JSONObject jobj = new JSONObject();
        try {
            jobj.put("username", username.getText().toString());
            jobj.put("password", hashed_pw);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jobj.toString();
    }

    public void saveData(JSONObject jobj) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();

        /* Faccio l'hash della password inserita dall'utente */
        String hashed_pw = null;
        try {
            hashed_pw = hashPassword(jobj.getString("password"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            editor.putString("name", jobj.getString("name"));
            editor.putString("surname", jobj.getString("surname"));
            editor.putString("company", jobj.getString("company"));
            editor.putString("piva", jobj.getString("piva"));
            editor.putString("phone", jobj.getString("phone"));
            editor.putString("email", jobj.getString("email"));
            editor.putString("username", jobj.getString("username"));
            editor.putString("password", hashed_pw.toString());
            editor.putBoolean("admin", jobj.getBoolean("admin"));
            editor.putString("address", jobj.getString("address"));

            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /* Cerchiamo se ci sono dati dell'utente salvati nelle preferenze. */
    public JSONObject retrieveData() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        JSONObject jobj = new JSONObject();

        try {
            jobj.put("name", sharedPref.getString("name", null));
            jobj.put("surname", sharedPref.getString("surname", null));
            jobj.put("company", sharedPref.getString("company", null));
            jobj.put("piva", sharedPref.getString("piva", null));
            jobj.put("phone", sharedPref.getString("phone", null));
            jobj.put("email", sharedPref.getString("email", null));
            jobj.put("username", sharedPref.getString("username", null));
            jobj.put("admin", sharedPref.getBoolean("admin", false));
            jobj.put("address", sharedPref.getString("address", null));


            if (jobj.getString("username") == null) {
                return null;
            } else {
                return jobj;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String hashPassword(String password) {
        MessageDigest md = null;
        byte[] digest = null;
        StringBuilder hashed_pw = new StringBuilder();
        try {
            md = MessageDigest.getInstance("SHA-512");
            digest = md.digest(password.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < digest.length; i++) {
            hashed_pw.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
        }

        return hashed_pw.toString();
    }
}
